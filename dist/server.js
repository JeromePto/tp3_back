"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var path_1 = __importDefault(require("path"));
var metrics_1 = require("./metrics");
var app = express_1.default();
var port = process.env.PORT || "8080";
app.set("views", path_1.default.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(express_1.default.static(path_1.default.join(__dirname, "public")));
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
var dbMet = new metrics_1.MetricsHandler("./db/metrics");
app.get("/metrics", function (req, res) {
    dbMet.read(function (err, result) {
        if (err)
            throw err;
        res.status(200).json(result);
    });
});
app.get("/metrics/:id", function (req, res) {
    dbMet.read(function (err, result) {
        if (err)
            throw err;
        res.status(200).json(result);
    }, req.params.id);
});
app.post("/metrics/:id", function (req, res) {
    dbMet.save(req.params.id, req.body, function (err) {
        if (err)
            throw err;
        res.status(200).send("OK");
    });
});
app.delete("/metrics/:id", function (req, res) {
    dbMet.delete(req.params.id, function (err) {
        if (err)
            throw err;
        res.status(200).send("OK");
    });
});
app.get("/hello/:name", function (req, res) {
    res.render("hello.ejs", { name: req.params.name });
});
app.get("/metrics.json", function (req, res) {
    metrics_1.MetricsHandler.get(function (err, result) {
        if (err)
            throw err;
        res.status(200).json(result);
    });
});
app.get("/", function (req, res) {
    res.write("Hello world");
    res.end();
});
app.listen(port, function (err) {
    if (err) {
        throw err;
    }
    console.log("server listening on http://localhost:" + port);
});
