import { LevelDB } from "./leveldb";
import WriteStream from "level-ws";

export class Metric {
    public timestamp: string;
    public value: number;

    constructor(ts: string, v: number) {
        this.timestamp = ts;
        this.value = v;
    }
}

export class MetricsHandler {
    private db: any;

    constructor(dbPath: string) {
        this.db = LevelDB.open(dbPath);
    }

    public save(
        key: number,
        metrics: Metric[],
        callback: (error: Error | null) => void
    ) {
        const stream = WriteStream(this.db);
        stream.on("error", callback);
        stream.on("close", callback);
        metrics.forEach((m: Metric) => {
            stream.write({
                key: `metric:${key}:${m.timestamp}`,
                value: m.value,
            });
        });
        stream.end();
    }

    public read(
        callback: (error: Error | null, result?: string[]) => void,
        id?: string
    ) {
        const result: string[] = new Array();
        const rs = this.db
            .createReadStream()
            .on("data", function(data) {
                // console.log(data.key, "=", data.value);
                if (id === undefined || data.key.split(":")[1] === id) {
                    // console.log(data.key, "=", data.value);
                    result.push(`${data.key} = ${data.value}`);
                }
            })
            .on("error", function(err) {
                console.log("Oh my!", err);
                callback(err);
            })
            .on("close", () => {
                callback(null, result);
            });
    }

    public delete(id: string, callback: (error: Error | null) => void) {
        const stream = this.db
            .createKeyStream()
            .on("data", data => {
                if (data.split(":")[1] === id) {
                    this.db.del(data, err => {
                        if (err) throw err;
                    });
                }
            })
            .on("error", err => {
                callback(err);
            })
            .on("close", () => {
                callback(null);
            });
    }

    static get(callback: (error: Error | null, result?: Metric[]) => void) {
        const result = [
            new Metric("2013-11-04 14:00 UTC", 12),
            new Metric("2013-11-04 14:30 UTC", 15),
        ];
        callback(null, result);
    }
}
