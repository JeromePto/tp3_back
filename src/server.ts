import express from "express";
import path from "path";

import { MetricsHandler } from "./metrics";

const app = express();
const port: string = process.env.PORT || "8080";

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, "public")));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const dbMet: MetricsHandler = new MetricsHandler("./db/metrics");

app.get("/metrics", (req: any, res: any) => {
    dbMet.read((err: Error | null, result?: string[]) => {
        if (err) throw err;
        res.status(200).json(result);
    });
});

app.get("/metrics/:id", (req: any, res: any) => {
    dbMet.read((err: Error | null, result?: string[]) => {
        if (err) throw err;
        res.status(200).json(result);
    }, req.params.id);
});

app.post("/metrics/:id", (req: any, res: any) => {
    dbMet.save(req.params.id, req.body, (err: Error | null) => {
        if (err) throw err;
        res.status(200).send("OK");
    });
});

app.delete("/metrics/:id", (req: any, res: any) => {
    dbMet.delete(req.params.id, (err: Error | null) => {
        if (err) throw err;
        res.status(200).send("OK");
    });
});

app.get("/hello/:name", (req: any, res: any) => {
    res.render("hello.ejs", { name: req.params.name });
});

// app.get("/metrics.json", (req: any, res: any) => {
//     MetricsHandler.get((err: Error | null, result?: any) => {
//         if (err) throw err;
//         res.status(200).json(result);
//     });
// });

app.get("/", (req: any, res: any) => {
    res.write("Hello world");
    res.end();
});

app.listen(port, (err: Error) => {
    if (err) {
        throw err;
    }
    console.log(`server listening on http://localhost:${port}`);
});
